package edu.epam.izhevsk.junit;

import static org.mockito.AdditionalMatchers.gt;
import static org.mockito.AdditionalMatchers.lt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

public class PaymentControllerTest {
	PaymentController paymentController;

	@InjectMocks
	AccountService mockAccountService;
	DepositService mockDepositService;

	@Spy
	PaymentController spyPaymentController;

	@Before
	public void initMocks() {
		mockAccountService = mock(AccountService.class);
		mockDepositService = mock(DepositService.class);

		when(mockAccountService.isUserAuthenticated(100L)).thenReturn(true);
		try {
			when(mockDepositService.deposit(lt(100L), anyLong())).thenReturn(
					"successful");
			when(mockDepositService.deposit(gt(100L), anyLong())).thenThrow(
					new InsufficientFundsException());
		} catch (InsufficientFundsException e) {
			e.printStackTrace();
		}

		paymentController = new PaymentController(mockAccountService,
				mockDepositService);
		spyPaymentController = spy(paymentController);
	}

	@Test
	public void testSuccessfulDeposit() throws InsufficientFundsException {
		paymentController.deposit(50L, 100L);
		verify(mockAccountService, times(1)).isUserAuthenticated(100L);
	}

	@Test(expected = SecurityException.class)
	public void testFailedDepositForUnauthenticatedUser()
			throws InsufficientFundsException {
		paymentController.deposit(50L, 1000L);
		verify(mockAccountService, times(1)).isUserAuthenticated(100L);
	}

	@Test(expected = InsufficientFundsException.class)
	public void testFailedDepositOfLargeAmount()
			throws InsufficientFundsException {
		paymentController.deposit(500L, 100L);
		verify(mockAccountService, times(1)).isUserAuthenticated(100L);
	}
}
